# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.wizard import StateTransition, Wizard

from .exceptions import PeriodValidationError

STATES = {'readonly': Eval('state') != 'draft'}


class Period(Workflow, ModelSQL, ModelView):
    "Period"
    __name__ = 'staff.payroll.period'
    _rec_name = 'name'
    name = fields.Char('Name', states=STATES,
            required=True)
    sequence = fields.Integer('Sequence', states=STATES,
            required=True)
    start = fields.Date('Start', required=True, states=STATES,
            domain=[('start', '<=', Eval('end', None))],
            depends=['end'])
    end = fields.Date('End', required=True, states=STATES,
            domain=[('end', '>=', Eval('start', None))],
            depends=['start'])
    state = fields.Selection([
            ('draft', 'Draft'),
            ('open', 'Open'),
            ('closed', 'Closed'),
            ], 'State', readonly=True)
    description = fields.Char('Description', states=STATES)

    @classmethod
    def __setup__(cls):
        super(Period, cls).__setup__()
        cls._order.insert(0, ('sequence', 'ASC'))
        cls._transitions |= set((
                ('draft', 'open'),
                ('open', 'draft'),
                ('open', 'closed'),
                ))
        cls._buttons.update({
                'open': {
                    'invisible': Eval('state') != 'draft',
                    },
                'close': {
                    'invisible': Eval('state') != 'open',
                    },
                'draft': {
                    'invisible': Eval('state') != 'open',
                    },
                })

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(self, periods):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(self, periods):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def close(self, periods):
        pass

    @classmethod
    def validate(cls, periods):
        super(Period, cls).validate(periods)
        for period in periods:
            period.check_start_end()
            # period.check_date_consistent()

    def check_start_end(self):
        if self.start > self.end:
            raise PeriodValidationError(gettext('staff_payroll.msg_wrong_start_end'))

    def check_date_consistent(self):
        periods = self.search([
                ('id', '!=', self.id), ['OR',
                [
                    ('start', '>=', self.start),
                    ('start', '<=', self.end),
                ], [
                    ('end', '>=', self.start),
                    ('end', '<=', self.end),
                ], [
                    ('start', '<=', self.start),
                    ('end', '>=', self.end),
                ],
            ]])
        if periods:
            raise PeriodValidationError(gettext('staff_payroll.msg_wrong_period_overlap'))


class OpenPeriod(Wizard):
    "Open Period"
    __name__ = 'staff.payroll.open_period'
    start_state = 'open_period'
    open_period = StateTransition()

    def transition_open_period(self):
        ids = Transaction().context['active_ids']
        Period = Pool().get('staff.payroll.period')
        if ids:
            period = Period(ids[0])
            Period.write([period], {'state': 'open'})
        return 'end'
