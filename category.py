# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields


class EmployeeCategory(ModelSQL, ModelView):
    "Employee Category"
    __name__ = 'staff.employee_category'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    wages_default = fields.Many2Many('employee_category-staff.wages',
            'employee_category', 'wage_type', 'Category - Wages Default')


class CategoryWagesDefault(ModelSQL):
    "Employee Category - Wages Default"
    __name__ = 'employee_category-staff.wages'
    _table = 'employee_category_staff_wages'
    employee_category = fields.Many2One('staff.employee_category',
            'Employee Category', required=True,
            ondelete='CASCADE')
    wage_type = fields.Many2One('staff.wage_type', 'Wage Type',
            required=True, ondelete='CASCADE')
