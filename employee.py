# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import StateTransition, Wizard

from .exceptions import MissingPartyWageType


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'
    category = fields.Many2One('staff.employee_category', 'Category')
    mandatory_wages = fields.One2Many('staff.payroll.mandatory_wage',
                                      'employee', 'Mandatory Wage')

    @classmethod
    def __setup__(cls):
        super(Employee, cls).__setup__()

    def get_fix_amount_wage_type(self, wage_id):
        fix_amount = sum([w.fix_amount or 0 for w in self.mandatory_wages if w.wage_type.id == wage_id])
        return fix_amount

    def get_last_date_futhermore(self):
        if self.contract and self.contract.futhermores:
            values = [f.end_date for f in self.contract.futhermores if f.end_date and f.state != 'draft']
            last_date_futhermore = max(values) if values else None
            if last_date_futhermore:
                return last_date_futhermore
            else:
                return self.contract.end_date or None

    @classmethod
    def create_mandatory_wages(cls, employees_ids):
        MandatoryWage = Pool().get('staff.payroll.mandatory_wage')
        employees = cls.browse(employees_ids)
        values = []
        for employee in employees:
            if employee.category and employee.category.wages_default:
                current_wages_ids = [m.wage_type.id
                                     for m in employee.mandatory_wages]
                for wage in employee.category.wages_default:
                    if wage.id in current_wages_ids:
                        continue
                    val = {
                        'employee': employee.id,
                        'wage_type': wage.id,
                    }
                    values.append(val)
        if values:
            MandatoryWage.create(values)


class MandatoryWage(ModelSQL, ModelView):
    "Mandatory Wage"
    __name__ = 'staff.payroll.mandatory_wage'
    employee = fields.Many2One('company.employee', 'Employee')
    wage_type = fields.Many2One('staff.wage_type', 'Wage Type',
                                required=True, domain=[('active', '=', True)])
    party = fields.Many2One('party.party', 'Party',
                            domain=[('active', '=', True)])
    fix_amount = fields.Numeric('Fix Amount')

    @classmethod
    def __setup__(cls):
        super(MandatoryWage, cls).__setup__()
        cls._order.insert(0, ('id', 'ASC'))

    @classmethod
    def validate(cls, mandatoryWages):
        for wage in mandatoryWages:
            wage.check_partys()

    def check_partys(self):
        pool = Pool()
        Wage = pool.get('staff.wage_type')
        if self.wage_type:
            wages = Wage.search([
                ('id', '=', self.wage_type.id),
            ])
            for wage in wages:
                if wage.party_required and not self.party:
                    raise MissingPartyWageType(
                        gettext('staff_payroll.msg_party_required', wage=wage.name))


class CreateMandatoryWages(Wizard):
    "Create Mandatory Wages"
    __name__ = 'staff.payroll.create_mandatory_wages'
    start_state = 'create_wages'
    create_wages = StateTransition()

    def transition_create_wages(self):
        Employee = Pool().get('company.employee')
        employees_ids = Transaction().context['active_ids']
        Employee.create_mandatory_wages(employees_ids)
        return 'end'
