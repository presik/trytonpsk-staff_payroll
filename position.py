# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool
from trytond.i18n import gettext
from .exceptions import MissingConfigPosition


class Position(metaclass=PoolMeta):
    __name__ = 'staff.position'
    workday_definition = fields.One2Many('staff.workday_definition',
                'position', 'Workday Definition')

    @classmethod
    def __setup__(cls):
        super(Position, cls).__setup__()
        cls._buttons.update({
                'create_workdays': {
                    'invisible': Bool(Eval('workday_definition')),
                },
        })

    def _default_workdays(self):
        pool = Pool()
        Workday = pool.get('staff.workday_definition')
        Config = pool.get('staff.configuration')
        config = Config(1)
        if not config.default_hour_workday or not config.default_hour_workday:
            raise MissingConfigPosition(gettext('staff_payroll.msg_missing_config_default'))
        for day in Workday.weekday.selection:
            values = {
                'position': self.id, 'weekday': day[0],
                'workday': config.default_hour_workday,
                'restday': config.default_restday
            }
            Workday.create([values])

    @classmethod
    @ModelView.button
    def create_workdays(cls, records):
        for rec in records:
            rec._default_workdays()


class WorkdayDefinition(ModelSQL, ModelView):
    'Workday Definition'
    __name__ = 'staff.workday_definition'
    position = fields.Many2One('staff.position', 'Position',
            required=True, ondelete='CASCADE')
    workday = fields.Numeric('Workday',  digits=(6, 4), required=True)
    restday = fields.Numeric('Restday',  digits=(2, 2), required=True)
    weekday = fields.Selection([
            ('monday', 'Monday'),
            ('tuesday', 'Tuesday'),
            ('wednesday', 'Wednesday'),
            ('thursday', 'Thursday'),
            ('friday', 'Friday'),
            ('saturday', 'Saturday'),
            ('sunday', 'Sunday'),
            ], 'Weekday', required=True)
    note = fields.Text('Note')

    @classmethod
    def __setup__(cls):
        super(WorkdayDefinition, cls).__setup__()
