# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.exceptions import UserError, UserWarning
from trytond.model.exceptions import ValidationError


class WageTypeValidationError(ValidationError):
    pass


class MissingPartyWageType(WageTypeValidationError):
    pass


class PayrollDeleteError(ValidationError):
    pass


class PayrollExistPeriodError(UserError):
    pass


class PayrollPeriodCloseError(ValidationError):
    pass


class PayrollMissingSequence(ValidationError):
    pass


class PayrollValidationError(UserError):
    pass


class PeriodValidationError(UserError):
    pass


class MissingConfigPosition(UserError):
    pass
