# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Id


def default_func(field_name):
    @classmethod
    def default(cls, **pattern):
        return getattr(
            cls.multivalue_model(field_name),
            'default_%s' % field_name, lambda: None)()
    return default


class StaffConfiguration(metaclass=PoolMeta):
    __name__ = 'staff.configuration'
    staff_payroll_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Payroll Sequence', required=True, domain=[
                ('sequence_type', '=',
                    Id('staff_payroll', 'sequence_type_payroll'))]),
        )
    default_hour_workday = fields.Numeric('Default Hour Workday',
            required=True, help='In hours', digits=(5, 4))
    default_restday = fields.Integer('Default Restday', required=True,
            help='In hours')
    default_liquidation_period = fields.Integer('Default Liquidation Period',
            required=True, help='In days')
    week_hours_work = fields.Integer('Week Hours Work', required=True,
            help='In hours')
    default_journal = fields.Many2One('account.journal', 'Default Journal',
            required=True)

    default_staff_payroll_sequence = default_func('staff_payroll_sequence')
    expense_contribution_entity = fields.Boolean('Expense Contribution Entity')
    minimum_salary = fields.Integer('Minimum Salary', required=True)

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in ['staff_payroll_sequence', 'staff_contract_sequence']:
            return pool.get('staff.configuration.sequence')
        return super(StaffConfiguration, cls).multivalue_model(field)

    @classmethod
    def __register__(cls, module_name):
        super().__register__(module_name)

        table = cls.__table_handler__(module_name)

        # Migration from > 6.0.8: change type column
        if table.column_is_type('default_hour_workday', 'int4'):
            table.alter_type('default_hour_workday', 'numeric')


class StaffConfigurationSequence(metaclass=PoolMeta):
    __name__ = 'staff.configuration.sequence'
    staff_payroll_sequence = fields.Many2One(
        'ir.sequence', "Staff Payroll Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=', Id('staff_payroll', 'sequence_type_payroll')),
            ],
        depends=['company'])

    @classmethod
    def default_staff_payroll_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('staff', 'sequence_staff_payroll')
        except KeyError:
            return None
